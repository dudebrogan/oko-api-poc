const express = require("express")
const app = express()
const util = require( 'util' )
const mysql = require( 'mysql' )
const bodyParser = require('body-parser')
const caseConverter = require('case-converter')
const constants = require('./constants')
const {format} = require("util");
const { Storage } = require("@google-cloud/storage");
const Multer = require('multer')
const vision = require('@google-cloud/vision').v1;
const client = new vision.ImageAnnotatorClient();

let processFile = Multer({
    storage: Multer.memoryStorage()
}).single('file')

let processFileMiddleware = util.promisify(processFile)

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*")
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
    next()
})

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
// app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use(processFileMiddleware);
app.use(express.static('public'));

const storage = new Storage({ keyFilename: "gcloud.json" });
const bucket = storage.bucket('oko-poc-test-file-uploads');
function makeDb( config ) {
    const connection = mysql.createConnection( config )
    return {
        query( sql, args ) {
            return util.promisify( connection.query )
                .call( connection, sql, args )
        },
        close() {
            return util.promisify( connection.end ).call( connection )
        }
    }
}

const dbSocketPath = process.env.DB_SOCKET_PATH || '/cloudsql';

const config = {
    host     : process.env.MYSQL_ENDPOINT ?? '127.0.0.1',
    port     : process.env.MYSQL_PORT ?? 3308,
    database : process.env.MYSQL_DATABASE ?? 'oko',
    user     : process.env.MYSQL_USERNAME ?? 'root',
    password : process.env.MYSQL_PASSWORD ?? 'O{}$s75LAe#xcW5p}I74LS',
    ...(process.env.INSTANCE_CONNECTION_NAME && {socketPath: `${dbSocketPath}/${process.env.INSTANCE_CONNECTION_NAME}`}),
}
app.use(bodyParser.json({ extended: true }))

app.post('/updateusertutorial', async (req, res) => {
    const { id, tutorialCompleted } = req.body
    let db = makeDb(config)

    let results
    try {
        results = await db.query(`update user_info set user_info.tutorial_completed = ${tutorialCompleted} where id=${id}`)
        res.sendStatus(200)
    } catch ( err ) {
        console.error('error updating user', err)
        res.sendStatus(400)
    }
})

app.post('/completeSignup', async (req, res) => {
    const { fullName, email } = req.body

    let db = makeDb(config)

    let results
    try {
        await db.query(`insert into user_info (first_name, last_name, company_id) values('${fullName.firstName}', '${fullName.lastName}', 1)`)
        results = await db.query(`SELECT * FROM user_info WHERE id= LAST_INSERT_ID()`)
        await db.query(`insert into email (user_id, email) values (${results[0].id}, ${email})`)
        res.status(200).send(caseConverter.toCamelCase(results[0]))
    } catch ( err ) {
        console.error('error updating user', err)
        res.sendStatus(400)
    }

})

app.get('/getUserStatus', async (req, res) => {

    const {email} = req.query
    if (!email?.length) res.status(400).send('must send an email')
    let db = makeDb(config)
    let results
    try {
        results = await db.query(`select ui.id, ui.first_name, ui.last_name, ui.company_id, e.email_address, ui.tutorial_completed from user_info as ui inner join email as e on e.user_id = ui.id where e.email_address = '${email}' limit 1`)
        results[0].tutorial_completed = results[0].tutorial_completed !== 0;
    } catch ( err ) {
        console.error('error getting user', err)
        res.status(400).send()
    } finally {
        await db.close()
    }

    res.status(200).send(caseConverter.toCamelCase(results[0]))
})

app.post('/become', async (req, res) => {

    const { id, type } = req.body
    let db = makeDb(config)

    let results
    try {
        if (type === constants.buyer) await db.query(`update user_info set company_id = 1 where id = ${id}`)
        if (type === constants.seller) await db.query(`update user_info set company_id = 2 where id = ${id}`)

        results = await db.query(`select ui.id, ui.first_name, ui.last_name, ui.company_id, e.email_address from user_info as ui inner join email as e on e.user_id = ui.id where e.user_id = '${id}' limit 1`)
        res.status(200).send(caseConverter.toCamelCase(results[0]))
    } catch ( err ) {
        console.error('become error', err)
        res.sendStatus(400)
        // handle the error
    } finally {
        await db.close()
    }
})

app.get('/transaction/documents/:transactionId', async (req, res) => {
    const {transactionId} = req.params

    let db = makeDb(config)

    try {
        results = await db.query(`select name from document where document.transaction_id = ${transactionId}`)
        res.status(200).send(caseConverter.toCamelCase({documents: results}))
    } catch (e) {
        console.error('error getting docs for transaction', e)
    }

})

app.get('/transactions', async (req, res) => {

    const {companyId, transactionType} = req.query // maybe shouldn't do transaction type, just get all transactions? we'll see as we get more product knowledge.
    if (!companyId?.length || !transactionType?.length) res.status(400).send('must send a company id and transaction type')
    let db = makeDb(config)
    let results
    try {
        results = await db.query(`select tr.id, tr.completed, tr.origin, tr.created, tr.price, tr.volume, tr.status, tr.rating, tr.quantity, tr.transaction_id from transaction as tr inner join company_${transactionType}_transaction as ct on tr.id = ct.transaction_id where ct.company_id = '${companyId}'`)
        results = await Promise.all(results.map(async item => {
            const documents = await db.query(`select name from document where document.transaction_id = ${item.id}`)
            return {
                ...item,
                documents
            }
        }))
    } catch ( err ) {
        // handle the error
    } finally {
        await db.close()
    }

    // console.log('results', results)

    res.status(200).send(caseConverter.toCamelCase({transactions: results}))
})

const confirmAssociatedTransaction = async (transactionName) => {
    let db = makeDb(config)

    try {
        const transactionId = await db.query(`select tr.id from transaction as tr where tr.transaction_id = '${transactionName}'`)

        if (!transactionId.length) throw new Error('noTransactionId')
        return transactionId
    } finally {
        await db.close()
    }
}

app.post('/saveFile', async(req, res) => {
    let db = makeDb(config)
    const { publicUrl, userId, name, transactionName } = req.body

    try {
        const transactionId = await confirmAssociatedTransaction(transactionName)

        await db.query(`insert into document (storage_path, uploader_id, name, transaction_id) values('${publicUrl}', '${userId}', '${name}', ${transactionId[0].id})`)

        const gcsSourceUri = `gs://oko-poc-test-file-uploads/${name}`;
        const gcsDestinationUri = `gs://oko-poc-test-file-uploads/results/`;
        const inputConfig = {
            // Supported mime_types are: 'application/pdf' and 'image/tiff'
            mimeType: 'application/pdf',
            gcsSource: {
                uri: gcsSourceUri,
            },
        };

        const outputConfig = {
            gcsDestination: {
                uri: gcsDestinationUri,
            },
        };
        const features = [{type: 'DOCUMENT_TEXT_DETECTION'}];
        const request = {
            requests: [
                {
                    inputConfig: inputConfig,
                    features: features,
                    outputConfig: outputConfig,
                    pages: [1]
                },
            ],
        };

        const [result] = await client.batchAnnotateFiles(request);
        // const [filesResponse] = await result.promise();
        // const destinationUri =
        //     filesResponse.responses[0].outputConfig.gcsDestination.uri;

        const responses = result.responses[0].responses;

        const words = []

        for (const response of responses) {
            for (const page of response.fullTextAnnotation.pages) {
                for (const block of page.blocks) {
                    for (const paragraph of block.paragraphs) {
                        for (const word of paragraph.words) {
                            const symbol_texts = word.symbols.map(symbol => symbol.text);
                            const word_text = symbol_texts.join('');
                            words.push(word_text)
                        }
                    }
                }
            }
        }
        res.status(200).send(words)
    } catch(e) {
        console.error('save file error', e)
        res.status(400).send({error: "Transaction Id not found"})
    } finally {
        await db.close()
    }


})

app.post('/upload', async (req, res) => {
    try {
        await processFileMiddleware(req, res);
        const { file } = req
        if (!file) {
            return res.status(400).send({ message: "Please upload a file!" });
        }

        const blob = bucket.file(file.originalname);
        const blobStream = blob.createWriteStream({
            resumable: false,
        });

        blobStream.on("error", (err) => {
            res.status(500).send({ message: err.message });
        });

        blobStream.on("finish", async (data) => {
            const publicUrl = format(
                `https://storage.googleapis.com/${bucket.name}/${blob.name}`
            );

            res.status(200).send({
                message: "Uploaded the file successfully: " + file.originalname,
                url: publicUrl,
            });
        });

        blobStream.end(file.buffer);
    } catch (err) {
        console.error('upload error', err);

        if (err.code === "LIMIT_FILE_SIZE") {
            return res.status(500).send({
                message: "File size cannot be larger than 2MB!",
            });
        }

        res.status(500).send({
            message: `Could not upload the file: ${file.originalname}. ${err}`,
        });
    }
})

app.get('/files', async(req, res) => {
    const { userId } = req.query
    let db = makeDb(config)

    const userFiles = await db.query(`select d.id, tr.transaction_id, d.name, d.storage_path from document as d inner join transaction as tr on d.transaction_id = tr.id where d.uploader_id = '${userId}'`)

    try {
        const [files] = await bucket.getFiles();
        let fileInfos = [];

        userFiles.forEach((fileInfo) => {
            fileInfos.push({
                name: fileInfo.name,
                url: fileInfo.storage_path,
                transactionId: fileInfo.transaction_id
            });
        });

        res.status(200).send(fileInfos);
    } catch (err) {
        console.error('get files error', err);

        res.status(500).send({
            message: "Unable to read list of files!",
        });
    } finally {
        await db.close()
    }
})

app.get('/files/:fileName', async (req, res) => {
    const { fileName } = req.params
    try {
        await bucket
            .file(`${fileName}`)
            .createReadStream()
            .on("error", (err) => {
                console.error(" --> rstream error: " + err)
            })
            .on("response", (strRsp) => {
                res.setHeader('Content-Length', strRsp.headers['content-length'])
                res.setHeader('Content-Type', strRsp.headers['content-type'])
                res.header('Content-Disposition', 'attachment; filename=' + req.params.filename)
            })
            .on("end", () => {
                res.status(200).end()
                return true
            })
            .pipe(res)

    } catch (err) {
        res.status(500).send({
            message: "Could not download the file. " + err,
        });
    } finally {
        await db.close()
    }
})



const port = process.env.PORT || 3000
app.listen(port, () => {
    console.log('Node API listening on port', port)
})
